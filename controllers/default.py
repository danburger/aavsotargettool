# -*- coding: utf-8 -*-
### required - do no delete
import time
import random

def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    session.secure()
    return get_targets()

@auth.requires_membership("admin")
def add_multiple():
    form = SQLFORM.factory(
        Field('targets', type="text", requires=IS_NOT_EMPTY()),
        Field('obs_section', type='list:string', comment="Hold the Ctrl key and click to select multiple types. On a Mac, hold the Command key and click.",
            label=T('Observing Section'),requires=IS_IN_SET(('Alerts / Campaigns', 'Cataclysmic Variables', 'Eclipsing Variables', 'Short Period Pulsators', 'Long Period Variables', 'Young Stellar Objects', 'High Energy Targets', 'Exoplanets', 'None'),multiple=True),default='None',required=True),
        Field('note', type="text", requires=IS_NOT_EMPTY(), label=T('Add note to all targets')),
        Field('priority', type="boolean", requires=IS_NOT_EMPTY(), label=T('Make all targets high priority')))
    if form.process().accepted:
        add_targets(form.vars.targets,form.vars.obs_section,form.vars.note,form.vars.priority)
        response.flash = 'targets will be added shortly'
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)
    
@auth.requires_membership('admin')
def delete_multiple():
    if len(request.vars) > 0:
        for i in request.vars:
            if request.vars[i] == "on":
                db(db.targets.id==int(i)).delete()
        response.flash = "targets deleted"
    targets = db(db.targets.id>0).select()
    return dict(targets=targets)
    
@auth.requires_membership('admin')
def validate_target(form):
    starName = form.vars.star_name
        
    info = vsxAPI((starName,form.vars.zfilter))[0]        # Returns dictionary of parsed star datas from VSX Profile
    if type(info) is str:           # Check if vsxAPI returned error string
        form.errors.star_name = 'Incorrect star name. Please be sure you entered the name correctly.'
    
    if floatOrNone(form.vars.obs_cadence):
        obs_cadence = floatOrNone(form.vars.obs_cadence)
    elif info['period']:
        obs_cadence = info['period'] * 0.1
    else:
        obs_cadence = 3
    
    form.vars.zra = info['ra']
    form.vars.zdec = info['dec']
    form.vars.max_mag = info['maxMag']
    form.vars.max_mag_band = info['maxMagBand']
    form.vars.min_mag = info['minMag']
    form.vars.min_mag_band = info['minMagBand']
    form.vars.period = info['period']
    form.vars.var_type = info['varType']
    form.vars.last_data_point = info['obsJDate']
    form.vars.constellation = info['constellation']
    form.vars.obs_cadence = obs_cadence
    form.vars.vsx_url = 'https://www.aavso.org/vsx/index.php?view=detail.top&oid=' + str(info['OID'])
    
    #check for duplicates
    duplicates = db(db.targets.star_name.like(form.vars.star_name,case_sensitive=False))\
        (db.targets.zfilter == form.vars.zfilter)\
        (db.targets.obs_mode == form.vars.obs_mode)\
        (db.targets.obs_cadence==form.vars.obs_cadence).select()
    if (len(duplicates) > 0) or (len(duplicates) > 1 and len(request.args) > 0):
        form.errors.star_name = "This is a duplicate"
        form.errors.zfilter = "This is a duplicate"
        form.errors.obs_mode = "This is a duplicate"
        form.errors.obs_cadence = "This is a duplicate"

@auth.requires_membership('admin')
def tool():
    if len(request.args) > 0:
        form = SQLFORM(db.targets,int(request.args[0]),deletable=True)
    else:
        form = SQLFORM(db.targets)
    if form.process(onvalidation=validate_target).accepted:
        if len(request.args) > 0:
            redirect(URL("index"))
        else:
            response.flash = 'The target was submitted successfully.'
    elif form.errors:
        response.flash = 'The form has errors.'
    else:
        response.flash = 'Please fill out the form.'
    return dict(form=form)

def error():
    return dict()

def refresh():
    db(db.scheduler_task.task_name=="refresh_targets").delete()
    refresh_targets()
    return "Target refresh added to scheduler"
    
@auth.requires_membership('owner')
def userlist():
    users = db(db.auth_user.id>0).select()
    return dict(users=users)

@auth.requires_membership('owner')
def makeadmin():
    auth.add_membership('admin',request.args[0])
    redirect(URL("userlist"))

@auth.requires_membership('owner')
def removeadmin():
    auth.del_membership('admin',request.args[0])
    redirect(URL("userlist"))
    
@auth.requires_membership('owner')
def makeowner():
    auth.add_membership('owner',request.args[0])
    redirect(URL("userlist"))

@auth.requires_membership('owner')
def removeowner():
    auth.del_membership('owner',request.args[0])
    redirect(URL("userlist"))
    
def setup():
    if db(db.scheduler_task.id>0).count() == 0:
        scheduler.queue_task("refresh_data",repeats=0,period=60)
    return "Done"
    
def forgot():
    form = SQLFORM.factory(
        Field("user","string",required=True,label="Email"))
    if form.process().accepted:
        user = db(db.auth_user.email==request.vars.user).select().first()
        if user:
            passwordkey = ''.join(random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") for x in range(25))
            db(db.auth_user.id==user.id).update(reset_password_key=passwordkey)
            email = user.email
            name = user.first_name
            content = "Use this link to reset your password:"
            link = "http://%s/%s/default/forgot2/%s" % (request.env.http_host,request.application,passwordkey)
            subject = "Password recovery"
            button = "Reset password"
            response.flash = email_user(content,email,subject=subject,button=button,link=link,safety=False)
        else:
            response.flash = "Could not find user."
    elif form.errors:
        response.flash = 'Form has errors.'
    return dict(form=form)
    
def forgot2():
    user = db(db.auth_user.reset_password_key == request.args[0]).select().first()
    form = SQLFORM.factory(
        Field("password","password",required=True,label="Enter new password"),
        Field("password_two","password",required=True,label="Enter new password again"))
    if form.process().accepted:
        if request.vars.password and request.vars.password == request.vars.password_two:
            db(db.auth_user.id==user.id).validate_and_update(reset_password_key="",password=request.vars.password)
            redirect(URL("index"))
        elif not request.vars.password:
            response.flash = "Please enter a password."
        else:
            response.flash = "Passwords do not match."
    elif form.errors:
        response.flash = 'Form has errors.'
    return dict(form=form)
    
def feedback():
    form = SQLFORM(db.feedback)
    if auth.user_id:
        form.vars.name = auth.user.first_name + " " + auth.user.last_name
        form.vars.email = auth.user.email
    if form.process().accepted:
        email_subject = "%s sends feedback" % (form.vars.name or form.vars.email)
        email_content = '%s (%s) submitted the following feedback: %s' % (form.vars.name,form.vars.email,form.vars.zmessage)
        email_link = None
        email_button = None
        email_user(content=email_content,subject=email_subject,link=email_link,button=email_button,email="dan.burger@vanderbilt.edu",safety=False)
        email_subject = "Thank you for your feedback"
        email_content = "Thank you for your recently submitted feedback to the AAVSO Target Tool. Your message will be sent to members of our support team who should be able to respond to your feedback shortly. The AAVSO Target Tool is still in development so we are prioritizing our responses to questions on a selective basis, but generally within 7 days."
        email_user(content=email_content,subject=email_subject,email=form.vars.email, safety=False)
        response.flash = "We have received your feedback. Thank you for your response."
    elif form.errors:
        response.flash = 'form has errors'
    elif auth.user_id:
        form.vars.name = auth.user.first_name + " " + auth.user.last_name
        form.vars.email = auth.user.email
    return dict(form=form)
  
@auth.requires_membership('admin')  
def qc():
    targets = db(db.targets.id>0).select(limitby=(0,10), orderby="<random>")
    vsxdata = [vsxAPI((t.star_name,t.zfilter)) for t in targets]
    return dict(targets=targets,vsxdata=vsxdata)
    
@auth.requires_membership('admin')
def tracebacks():
    tracebacks = db(db.scheduler_run.id>0).select(db.scheduler_run.traceback)
    return "<br>".join([t.traceback.split("\n")[-2] for t in tracebacks if t.traceback])