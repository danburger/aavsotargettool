import datetime
import base64
import json
import calendar

def index():
    if auth.user_id:
        user = db(db.auth_user.id==auth.user_id).select().first()
    else:
        return dict(user=None)
    if not user.api_key or request.vars.new_key:
        api_key=''.join(random.choice("0123456789abcdef") for x in range(32))
        db(db.auth_user.id==auth.user_id).update(api_key=api_key)
        user.api_key=api_key
    if not auth.user.api_key_b64 or request.vars.new_key:
        api_key_b64 ="Basic " + base64.b64encode(user.api_key + ":api_token")
        db(db.auth_user.id==auth.user_id).update(api_key_b64=api_key_b64)
        user.api_key_b64=api_key_b64
    if user.api_requests is None or user.api_last_request is None or (user.api_last_request + API_PERIOD < request.now):
        user.api_requests = 0
        db(db.auth_user.id==auth.user_id).update(api_requests=0)
        user.api_last_request = request.now
        db(db.auth_user.id==auth.user_id).update(api_last_request=request.now)
    else:
        pass#auth.user.api_requests += 1
    return dict(user=user)

def v1():
    if not request.is_https:
        raise HTTP(400,'URL is not sent through HTTPS. Please replace HTTP with HTTPS in the URL')
    user = db(db.auth_user.api_key_b64 == request.env.HTTP_AUTHORIZATION).select().first()
    if not user:
        raise HTTP(401,'Invalid API key, or API key not provided')
    if user.api_requests >= API_LIMIT:
        raise HTTP(429,'Too many requests. User has exceeded quota')
    if user.api_last_request + API_PERIOD < request.now:
        db(db.auth_user.id==user.id).update(api_requests=1,api_last_request=request.now)
        response.headers["X-Rate-Limit-Limit"] = "1"
        response.headers["X-Rate-Limit-Remaining"] = str(API_LIMIT-1)
        response.headers["X-Rate-Limit-Reset"] = API_PERIOD.seconds
    else:
        db(db.auth_user.id==user.id).update(api_requests=user.api_requests+1)
        response.headers["X-Rate-Limit-Limit"] = str(user.api_requests+1)
        response.headers["X-Rate-Limit-Remaining"] = str(API_LIMIT-(user.api_requests+1))
        response.headers["X-Rate-Limit-Reset"] = API_PERIOD.seconds - (request.now-user.api_last_request).seconds
    verb = request.env.REQUEST_METHOD
    noun = request.args[0]
    if noun == "telescope":
        if verb == "POST":
            new_data = {}
            for attribute in request.vars.keys():
                if attribute in ['latitude','longitude','targetaltitude','sunaltitude']:
                    new_data[attribute] = float(request.vars.get(attribute))
                    user[attribute] = float(request.vars.get(attribute))
            db(db.auth_user.id==user.id).update(**new_data)
        result = {'latitude':user.latitude,
                  'longitude':user.longitude,
                  'targetaltitude':user.targetaltitude,
                  'sunaltitude':user.sunaltitude}
    if noun == "nighttime":
        if request.vars.latitude:
            latitude = float(request.vars.latitude)
        else:
            latitude = user.latitude
        if request.vars.longitude:
            longitude = float(request.vars.longitude)
        else:
            longitude = user.longitude
        if request.vars.time:
            usetime = datetime.datetime.utcfromtimestamp(float(request.vars.time))
        else:
            usetime = datetime.datetime.utcnow()
        if request.vars.sunaltitude:
            sunaltitude = float(request.vars.sunaltitude)
        else:
            sunaltitude = user.sunaltitude
        data = getSunriseSunsetTimes((latitude,longitude),sunHorizon=sunaltitude,usetime=usetime)
        status_codes = ['','SUN_IS_UP','SUN_IS_DOWN','SUN_IS_ALWAYS_UP','SUN_IS_ALWAYS_DOWN','SUN_ENTERS_ALWAYS_DOWN','SUN_EXITS_ALWAYS_DOWN']
        result = {'status':status_codes[data[0]]}
        if data[1]:
            result['sunset'] = calendar.timegm(data[1].datetime().timetuple())
        if data[2]:
            result['sunrise'] = calendar.timegm(data[2].datetime().timetuple())
    if noun == "targets":
        vars = request.vars
        if vars.obs_section:
            if type(vars.obs_section) == str:
                vars['obs_section'] = [vars.obs_section]
            for var in vars.obs_section:
                vars[var] = 'on'
                if var == "ep":
                    vars['exo'] = 'on'
                if var == 'eb':
                    vars['ev'] = 'on'
                if var == 'spp':
                    vars['spv'] = 'on'
                if var == 'all':
                    vars['ac'] = 'on'
                    vars['exo'] = 'on'
                    vars['cv'] = 'on'
                    vars['ev'] = 'on'
                    vars['spv'] = 'on'
                    vars['lpv'] = 'on'
                    vars['yso'] = 'on'
                    vars['het'] = 'on'
                    vars['misc'] = 'on'
            vars['settype'] = 'true'
        if vars.observable:
            vars['select'] = 'observable'
        else:
            vars['select'] = 'all'
        if vars['orderby'] == 'ra':
            vars['orderby'] = 'zra'
        if vars['orderby'] == 'dec':
            vars['orderby'] = 'zdec'
        if vars['orderby'] == 'filter':
            vars['orderby'] = 'zfilter'
        if request.vars.latitude:
            user.latitude = float(request.vars.latitude)
        if request.vars.longitude:
            user.longitude = float(request.vars.longitude)
        if request.vars.sunaltitude:
            user.sunaltitude = float(request.vars.sunaltitude)
        if request.vars.targetaltitude:
            user.targetaltitude = float(request.vars.targetaltitude)
        if request.vars.time:
            usetime = datetime.datetime.utcfromtimestamp(float(request.vars.time))
        else:
            usetime = datetime.datetime.utcnow()
        print repr(usetime)
        data = get_targets(userid=user.id,user=user,vars=vars,usetime=usetime)
        result = []
        for i in range(len(data['targets'])):
            target_data = data['targets'][i]
            observability_data = data['observabilityTimes'][target_data.star_name]
            observability_result = []
            for status, event_time in observability_data:
                if event_time:
                    event_timestamp = calendar.timegm(event_time.datetime().timetuple())
                else:
                    event_timestamp = None
                if status == TARGET_RISES:
                    observability_result.append(("TARGET_RISES",event_timestamp))
                if status == TARGET_SETS:
                    observability_result.append(("TARGET_SETS",event_timestamp))
                if status == TARGET_IS_ALWAYS_UP:
                    observability_result.append(("TARGET_IS_ALWAYS_UP",event_timestamp))
                if status == TARGET_IS_ALWAYS_DOWN:
                    observability_result.append(("TARGET_IS_ALWAYS_DOWN",event_timestamp))
            if target_data.last_data_point:
                last_data_point = calendar.timegm(target_data.last_data_point.timetuple())
            else:
                last_data_point = None
            result.append({
                'star_name': target_data.star_name,
                'ra': target_data.zra,
                'dec': target_data.zdec,
                'constellation': target_data.constellation,
                'var_type': target_data.var_type,
                'min_mag': target_data.min_mag,
                'min_mag_band': target_data.min_mag_band,
                'max_mag': target_data.max_mag,
                'max_mag_band': target_data.max_mag_band,
                'period': target_data.period,
                'obs_cadence': target_data.obs_cadence,
                'obs_mode': target_data.obs_mode,
                'obs_section': target_data.obs_section,
                'filter': target_data.zfilter,
                'other_info': target_data.other_info,
                'priority': target_data.priority,
                'constellation': target_data.constellation,
                'last_data_point': last_data_point,
                'observability_times': observability_result,
                'solar_conjunction': angsep(datetime_to_jd(usetime),target_data.zra,target_data.zdec) < 30
            })
        result = {'targets':result}
    response.headers['Content-Type'] = 'application/json'
    return json.dumps(result)