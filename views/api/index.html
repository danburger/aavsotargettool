{{extend 'layout.html'}}
{{import datetime}}
{{from gluon.tools import prettydate}}
{{import base64}}

<h2>AAVSO Target Tool API v1.0.0</h2>
<br/>


<div class="alert alert-info">
    {{if user:}}
    <label class="btn btn-link" for="api_key" style="float:left"><b>Your API key is:</b></label>
    <input id="api_key" name="api_key" type="text" style="width:300px;float:left" class="form-control" value="{{=user.api_key}}">
    <a class="btn btn-default" href="/{{=request.application}}/api/index?new_key=true">Generate new key</a>
    {{else:}}
    <b>An API key is required to access the API. To get one, <a href="/aavso/default/user/register?_next=/aavso/api">create an account</a> or <a href="/aavso/default/user/login?_next=/aavso/api">log in</a>.</b>
    {{pass}}
</div>

<dl class="dl-horizontal">


        

<dt>What is this?</dt>
<dd>The AAVSO Target Tool API allows you to write programs that access the AAVSO Target Tool outside of the browser. Let's say that you've written a program in Python to do computational work for your observations, and it takes in data from the AAVSO Target Tool as input. You could manually copy and paste data from the table and get your program to parse the data, but after many requests it can become tedious over time. Instead, with a few simple commands, you can use the API to request the data and it will come to you in an easy to use, machine-readable format.<br/><br/></dd>

<dt>Should I use this?</dt>
<dd>Many computational astronomers depend on APIs like this one for their work because it allows their code to access or update online data automatically, without going to the web interface by hand. The AAVSO Target Tool API will allow you to access targets and visibility information from the command line (outlined below). If you're not planning on writing code, or if you only need to look up a few targets, you may wish to use the <a href="/aavso">interactive table</a> instead.<br/><br/></dd>

<dt>What is an API?</dt>
<dd>An application programming interface (API) allows you to write code that interacts with websites. Most major websites provide an API as a courtesy to developers everywhere. For instance, <a href="https://developer.spotify.com/documentation/web-api/">here is what the API for Spotify looks like</a>.<br/><br/></dd>

<dt>How do I try it out?</dt>
<dd>Many programming languages have libraries that can access web APIs. We will focus on the <a href="http://www.python.org/">Python programming language</a> and the <a href="https://2.python-requests.org/en/master/">Requests library</a> for interfacing with APIs. Just like you need to put on socks before putting on shoes, you will need to <a href="http://www.python.org/">make sure Python is installed</a> before <a href="https://2.python-requests.org/en/master/">installing Requests</a>. (We've linked to Requests version 2; you're welcome to use Requests III instead, but keep in mind the syntax may be different.)
  <br/><br/>
  Once you've downloaded Python and Requests, type in "python3" in your terminal to get started. For Windows users, Python 3 should be in your Start menu.
  <br/><br/>
  <mark>We've highlighted some text in yellow like this; as you go through the documentation, all you have to do is copy and paste the commands provided and it will go through the API.</mark><br/><br/></dd>

<dt>Technical details</dt>
<dd>
  If this is your first time using an API and you're using Python and Requests, you may skip this section.<br/><br/>
{{if user:}}
You have used <b>{{=user.api_requests}} of {{=API_LIMIT}}</b> API requests, reseting every hour.
The next reset will be <b>{{=prettydate(user.api_last_request + API_PERIOD,T)}}.</b><br/>
{{else:}}
All users are restricted to <b>{{=API_LIMIT}}</b> requests per hour. Once you log in, the number of requests you have made in the past hour will show up here.</b><br/>
{{pass}}
Rate limit information can be accessed through the response headers:<br/>
<ul>
<li><code>X-Rate-Limit-Limit</code>: The number of allowed requests in the current period</li>
<li><code>X-Rate-Limit-Remaining</code>: The number of remaining requests in the current period</li>
<li><code>X-Rate-Limit-Reset</code>: The number of seconds left in the current period</li>
</ul>
Any requests exceeding the limit will return an HTTP 429 error.<br/>
<br/>
Authentication is via HTTP Basic Auth, using the API key as the username and "api_token" as the password. <b>Logging in to this page on your web browser does not log you in to the API.</b> Many API libraries have HTTP Basic Auth built in, but in case you need to do it manually:
<ul>
    <li>Take the string of the API key, followed by <code>:api_token</code>. {{if user:}}In your case it would look like this: <code>{{=user.api_key}}:api_token</code>{{pass}} </li>
    <li>Then encode it to base 64. {{if user:}}<code>{{=base64.b64encode(user.api_key+":api_token")}}</code>{{pass}}</li>
    <li>Then stick <code>Basic </code> in the front, including the space. {{if user:}}<code>Basic {{=base64.b64encode(user.api_key+":api_token")}}</code>{{pass}}</li>
    <li>Set the header <code>HTTP_AUTHORIZATION</code> to the resulting string and you should be good to go.</li>
</ul>
<br/>
<b>Only HTTPS traffic is allowed.</b> In the interest of security, any HTTP traffic will return a 400 error, without redirection to HTTPS.
<br/><br/>
All inputs are accepted as URL parameters and are optional unless otherwise marked.
<br/><br/>
All outputs are returned in JSON as long as they are valid (HTTP 200). Client errors (HTTP 4xx) are returned as plain text. Server errors (HTTP 5xx) are returned as the HTML for the "Internal error" page. Make sure your code is able to handle these error responses gracefully.
<br/><br/>
The API returns the following error codes:<br/>
200 = Valid code. Response is provided as JSON.<br/>
401 = Invalid API key, or API key not provided.<br/>
410 = API no longer in service. Check filtergraph.com/aavso or aavso.org for a new version.<br/>
429 = Too many requests. User has exceeded quota.<br/>
Other 4xx = Something is wrong with the query. An error message will be left in the response.<br/>
503 = Temporarily down for maintenance. Try again later.<br/>
Other 5xx = Server error. Check your query; if it looks OK then check {{=request.env.http_host}} to see if the server is up. It may come back later.<br/><br/>
</dd>

<dt>GET telescope</dt>
<dd>Gets latitude, longitude and other data of the current telescope.<br/>

<dl class="dl-horizontal">
<dt>Inputs</dt>
<dd>none</dd>
<dt>Outputs</dt>
<dd>
    <code>latitude</code> Latitude of telescope. South is negative, North is positive.<br/>
    <code>longitude</code> Longitude of telescope. West is negative, East is positive.<br/>
    <code>targetaltitude</code> Minimum altitude that the telescope can observe in degrees relative to the horizon.<br/>
    <code>sunaltitude</code> Altitude of sun at dusk and dawn in degrees.<br/>
</dd>
</dl>

<pre>
GET https://{{=request.env.http_host}}/aavso/api/v1/telescope
Authentication: Basic (username: {{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}, password: api_token)

Python code:
<mark>import requests
API_KEY = "{{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}"
entry = requests.get("https://{{=request.env.http_host}}/aavso/api/v1/telescope",auth=(API_KEY,"api_token"))
print(entry.json())</mark>

Result:
{'latitude':36.1661111111,'longitude':-86.7838888889,'targetaltitude':20,'sunaltitude':-5}
</pre>
<br/><br/></dd>

<dt>POST telescope</dt>
<dd>Sets latitude, longitude and other data of the current telescope.

<dl class="dl-horizontal">
<dt>Inputs</dt>
<dd>
    <code>latitude</code> Latitude of telescope. South is negative, North is positive.<br/>
    <code>longitude</code> Longitude of telescope. West is negative, East is positive.<br/>
    <code>targetaltitude</code> Minimum altitude that the telescope can observe in degrees relative to the horizon.<br/>
    <code>sunaltitude</code> Altitude of sun at dusk and dawn in degrees.<br/>
</dd>
<dt>Outputs</dt>
<dd>
    <code>latitude</code> Latitude of telescope. South is negative, North is positive.<br/>
    <code>longitude</code> Longitude of telescope. West is negative, East is positive.<br/>
    <code>targetaltitude</code> Minimum altitude that the telescope can observe in degrees relative to the horizon.<br/>
    <code>sunaltitude</code> Altitude of sun at dusk and dawn in degrees.<br/>
</dd>
</dl>

<pre>
POST https://{{=request.env.http_host}}/aavso/api/v1/telescope
Authentication: Basic (username: {{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}, password: api_token)

Python code:
<mark>import requests
API_KEY = "{{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}"
data = {'latitude':36.1661111111,'longitude':-86.7838888889}
entry = requests.post("https://{{=request.env.http_host}}/aavso/api/v1/telescope",auth=(API_KEY,"api_token"),data=data)
print(entry.json())</mark>

Result:
{'latitude':36.1661111111,'longitude':-86.7838888889,'targetaltitude':20,'sunaltitude':-5}
</pre>
<br/></dd>

<dt>GET nighttime</dt>
<dd>Gets nighttime period following a specified time for a specified location. All times are UTC.<br/>

<dl class="dl-horizontal">
<dt>Inputs</dt>
<dd>
    <code>latitude</code> Latitude of telescope. South is negative, North is positive. If not provided, the user's settings are assumed.<br/>
    <code>longitude</code> Longitude of telescope. West is negative, East is positive. If not provided, the user's settings are assumed.<br/>
    <code>sunaltitude</code> Altitude of sun at dusk and dawn in degrees. If not provided, the user's settings are assumed.<br/>
    <code>time</code> Time of interest as a UTC timestamp. If not provided, the current time is assumed.
</dd>
<dt>Outputs</dt>
<dd>
    <code>status</code> One of the status codes below.<br/>
    <code>sunset</code> If relevant, the sunset time as a UTC timestamp.<br/>
    <code>sunrise</code> If relevant, the sunrise time as a UTC timestamp.
</dd>
<dt>Status codes</dt>
<dd>
    <code>SUN_IS_UP</code> Sun is currently up; next sunset and the following sunrise are listed<br/>
    <code>SUN_IS_DOWN</code> Sun is currently down; next sunrise is listed<br/>
    <code>SUN_IS_ALWAYS_UP</code> 24 hour darkness; typically seen near the poles in winter<br/>
    <code>SUN_IS_ALWAYS_DOWN</code> 24 hour daylight; typically seen near the poles in summer<br/>
    <code>SUN_ENTERS_ALWAYS_DOWN</code> Sun is about to set into 24 hour darkness; sunset time is listed<br/>
    <code>SUN_EXITS_ALWAYS_DOWN</code> Sun is about to rise after 24 hour darkness period; sunrise time is listed
</dd>
</dl>

<pre>
GET https://{{=request.env.http_host}}/aavso/api/v1/nighttime
Authentication: Basic (username: {{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}, password: api_token)

Python code:
<mark>import requests
API_KEY = "{{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}"
entry = requests.get("https://{{=request.env.http_host}}/aavso/api/v1/nighttime",auth=(API_KEY,"api_token"))
print(entry.json())</mark>

Result:
{'status': 'SUN_IS_UP', 'sunrise': 1526553112, 'sunset': 1526518490}
<kbd>At the time this was run: the sun was up, the sunset time was 7:54pm and the sunrise time was 5:31am the very next day.</kbd>
</pre>
<br/></dd>

<dt>GET targets</dt>
<dd>Get information about targets in the Target Tool database. 

<dl class="dl-horizontal">
<dt>Inputs</dt>
<dd>
    <code>obs_section</code> An array with observing sections of interest. You may use one or more of: ac,ep,cv,eb,spp,lpv,yso,het,misc,all. Default is ['ac'] (Alerts & Campaigns).<br/>
    <code>observable</code> If true, filters out targets which are visible at the telescope location during the following nighttime period. Default is false.<br/>
    <code>orderby</code> Order by any of the output fields below, except for observability_times and solar_conjunction.<br/>
    <code>reverse</code> If true, reverses the order. Default is false.<br/>
    <code>latitude</code> Latitude of telescope. South is negative, North is positive. If not provided, the user's settings are assumed.<br/>
    <code>longitude</code> Longitude of telescope. West is negative, East is positive. If not provided, the user's settings are assumed.<br/>
    <code>targetaltitude</code> Minimum altitude that the telescope can observe in degrees relative to the horizon. If not provided, the user's settings are assumed.<br/>
    <code>sunaltitude</code> Altitude of sun at dusk and dawn in degrees. If not provided, the user's settings are assumed.<br/>
    <code>time</code> Time of interest as a UTC timestamp. If not provided, the current time is assumed.
</dd>
<dt>Outputs</dt>
<dd>
    <code>star_name</code> Name of the star as it appears in the Target Tool.<br/>
    <code>ra</code> Right ascension. Note this returns decimal degrees, not DMS.<br/>
    <code>dec</code> Declination. Note this returns decimal degrees, not DMS.<br/>
    <code>var_type</code> Variable type.<br/>
    <code>min_mag</code> Minimum magnitude.<br/>
    <code>min_mag_band</code> Minimum magnitude band.<br/>
    <code>max_mag</code> Maximum magnitude.<br/>
    <code>min_mag_band</code> Maximum magnitude band.<br/>
    <code>period</code> Period in days.<br/>
    <code>obs_cadence</code> Observing cadence in days.<br/>
    <code>obs_mode</code> Observing mode: Photometry, Visual, Spectroscopy, All<br/>
    <code>obs_section</code> Observing section: one or more of Alerts / Campaigns, Cataclysmic Variables, Eclipsing Variables, Short Period Pulsators, Long Period Variables, Young Stellar Objects, High Energy Targets, Exoplanets, Miscellaneous, None.<br/>
    <code>filter</code> Filters: one or more of B, V, R, I, U, J, H, Vis, Visual, Spectroscopy, All<br/>
    <code>other_info</code> Info manually supplied by AAVSO about this target. Targets may be formatted with a URL, which is enclosed in double brackets along with a description: <code>[[Alert Notice 631 https://www.aavso.org/aavso-alert-notice-631]]</code><br/>
    <code>last_data_point</code> Last observation made in UTC.<br/>
    <code>priority</code> If true, designates a high priority target.<br/>
    <code>constellation</code> Constellation area where the target is located.<br/>
    <code>observability_times</code> Visibility events for the target in UTC, given telescope information and assuming target is not near solar conjunction. Provided in pairs of status code and UTC timestamp.<br/>
    <code>solar_conjunction</code> If true, target is near solar conjunction.<br/>
</dd>
<dt>Status codes</dt>
<dd>
    <code>TARGET_RISES</code> Time when the target becomes visible.<br/>
    <code>TARGET_SETS</code> Time when the target is no longer visible.<br/>
    <code>TARGET_IS_ALWAYS_UP</code> Target will be visible throughout the next nighttime period. If the sun is down, target will be visible for the rest of the current nighttime period.<br/>
    <code>TARGET_IS_ALWAYS_DOWN</code> Target will not be visible during the next nighttime period.<br/>
</dd>
</dl>
<pre>
GET https://{{=request.env.http_host}}/aavso/api/v1/targets
Authentication: Basic (username: {{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}, password: api_token)
        
Python code:
<mark>import requests
API_KEY = "{{if user:}}{{=user.api_key}}{{else:}}your-api-key-here{{pass}}"
entry = requests.get("https://{{=request.env.http_host}}/aavso/api/v1/targets",auth=(API_KEY,"api_token"),params={'obs_section':['all']})
print(entry.json())</mark>

Result:
{'targets':[
   ...,
   {'constellation': 'Cyg',
        'dec': 50.24142,
        'filter': '',
        'last_data_point': 1518435383,
        'max_mag': 5.6,
        'max_mag_band': 'V',
        'min_mag': 10.1,
        'min_mag_band': 'V',
        'obs_cadence': 3.0,
        'obs_mode': 'All',
        'obs_section': ['None'],
        'observability_times': [['TARGET_RISES', 1526524965]],
        'other_info': '',
        'period': None,
        'priority': False,
        'ra': 291.13779,
        'solar_conjunction': False,
        'star_name': 'CH Cyg',
        'var_type': 'ZAND+SR'},
    {'constellation': 'Oph',
        'dec': -24.9655,
        'filter': 'All',
        'last_data_point': 1501308179,
        'max_mag': 14.1,
        'max_mag_band': 'V',
        'min_mag': 22.0,
        'min_mag_band': 'V',
        'obs_cadence': 3.0,
        'obs_mode': 'All',
        'obs_section': ['None'],
        'observability_times': [['TARGET_RISES', 1526534623],
            ['TARGET_SETS', 1526551958]],
        'other_info': '',
        'period': None,
        'priority': False,
        'ra': 264.94208,
        'solar_conjunction': False,
        'star_name': 'Nova Oph 2017',
        'var_type': 'N'},
    ...
    ]
}
</pre>
<br/></dd>

<dt>Reference</dt>
<dd>This API is modeled on the <a href="https://github.com/toggl/toggl_api_docs/">Toggl API</a>.<br/>
Further reference: <a href="https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api">here</a>
and <a href="https://semver.org/">here</a></dd>

</dl>