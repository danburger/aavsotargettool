from xml.dom import minidom
from jdutil import jd_to_datetime
import requests

def vsxAPI(STAR_NAME,CHUNK_SIZE = 1):
    #ERROR = 'not_found'
    
    if type(STAR_NAME) == list: # VSX API to retrieve multiple stars at once
        result = []
        for i in range(0,len(STAR_NAME),CHUNK_SIZE):
            get_data = vsxAPI(STAR_NAME[i])
            result.append(get_data)
        return result
    elif type(STAR_NAME) == tuple:
        STAR_URL = 'https://www.aavso.org/vsx/index.php?view=api.object&last&ident=' + STAR_NAME[0].replace("+", "%2B").replace(" ", "+") + "&band=" + STAR_NAME[1]
    else:
        # From VSX API documentation
        STAR_URL = 'https://www.aavso.org/vsx/index.php?view=api.object&last&ident=' + STAR_NAME.replace("+", "%2B").replace(" ", "+")
        
    STAR_URL = STAR_URL.replace("&band=Visual","&band=Vis.").replace("&band=All","")
    print "Receiving "+STAR_URL

    xmltext = requests.get(STAR_URL).text
    allowedChars = [chr(n) for n in range(32, 127)]
    xmltext = "".join([char for char in xmltext if char in allowedChars])
    
    if '<Name>' not in xmltext:
        return "Unable to add "+repr(STAR_NAME)+"."
   
    print "Parsing"
    
    try:
        xmldata = minidom.parseString(xmltext.encode('utf-8'))
    except ExpatError:
        return "Unable to add "+repr(STAR_NAME)+"."
    
    if type(STAR_NAME) == list:      # VSX API to retrieve multiple stars at once
        starList = [parseStar(xmldata, k) for k in range(len(STAR_NAME))]
        return starList
        
    else:
        return [parseStar(xmldata)]
        
def parseStar(xmldata, i=0):
    print "Parsing star "+getElement(xmldata, 'Name', i)
    star_info = {   'OID': intOrNone(getElement(xmldata, 'OID', i)),     # Parse the data region for each attribute
                    'name': getElement(xmldata, 'Name', i),
                    'constellation': getElement(xmldata, 'Constellation', i),
                    'varType': getElement(xmldata, 'VariabilityType', i),
                    'maxMag': floatOrNone(split(getElement(xmldata, 'MaxMag', i), 0)),
                    'maxMagBand': split(getElement(xmldata, 'MaxMag', i), 1),
                    'epoch': intOrNone(getElement(xmldata, 'Epoch', i)),
                    'minMag': floatOrNone(split(getElement(xmldata, 'MinMag', i), 0)),
                    'minMagBand': split(getElement(xmldata, 'MinMag', i), 1),
                    'period': floatOrNone(getElement(xmldata, 'Period', i)),
                    'ra': floatOrNone(getElement(xmldata, 'RA2000', i)),
                    'dec': floatOrNone(getElement(xmldata, 'Declination2000', i)),
                    'obsJDate': jd_to_datetime(floatOrNone(getElement(xmldata, 'JD', i))) }
    return star_info


def getElement(xml, name, item=0):
    try:
        res = xml.getElementsByTagName("VSXObject")[item].getElementsByTagName(name)[0].childNodes[0].nodeValue
    except (AttributeError, IndexError):
        res = None
        
    return res
    
    
def split(element, num):
    try:
        res = element.split()[num]
    except (AttributeError, IndexError):
        res = None
    
    return res

    
def floatOrNone(value):
    try:
       res = float(value)
       return res
    except (ValueError, TypeError) as e:
        res = None
    
    try:                            # Check for a value like '<16.78'
        res2 = float(value[1:])
        return res2
    except (ValueError, TypeError) as e:
        res2 = None
    
    if ((res == None) and (res2 != None)):      
        res = res2
    
    return res
    
    
def intOrNone(value):
    try:
        res = int(value)
    except (ValueError, TypeError) as e:
        res = None
    
    return res