import os, ConfigParser
config = ConfigParser.RawConfigParser()
config.read(os.path.join(request.env.gluon_parent,"filtergraph.cfg"))

try:
    FG_MAILJET_APIKEY = config.get("mailjet","apikey")
except:
    FG_MAILJET_APIKEY = None
    
try:
    FG_MAILJET_SECRETKEY = config.get("mailjet","secretkey")
except:
    FG_MAILJET_SECRETKEY = None
    
#try:
FG_MAILJET_FROM = config.get("mailjet","from")
#except:
#    FG_MAILJET_FROM = None