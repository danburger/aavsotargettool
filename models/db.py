# -*- coding: utf-8 -*-

import random
import datetime

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

#if request.global_settings.web2py_version < "2.14.1":
#    raise HTTP(500, "Requires web2py 2.13.3 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
if not (request.is_https or request.env.remote_addr == "127.0.0.1") and request.env.http_host and request.url:
    if "api/v" in request.url:
        raise HTTP(400,'URL is not sent through HTTPS. Please replace HTTP with HTTPS in the URL')
    redirect("https://" + request.env.http_host.split(":")[0] + request.url)

# -------------------------------------------------------------------------
# app configuration made easy. Look inside private/appconfig.ini
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
#myconf = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL('sqlite://storage.sqlite',
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# now start the scheduler
# -------------------------------------------------------------------------
from gluon.scheduler import Scheduler
scheduler = Scheduler(db)

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = ['*'] if request.is_local else []
# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = 'bootstrap3_inline' #myconf.get('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = ''#myconf.get('forms.separator') or ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

from gluon.tools import Auth, Service, PluginManager

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db)

auth.settings.expiration = 2592100  # seconds
auth.settings.long_expiration = 2592100  # seconds
auth.settings.remember_me_form = False

auth.settings.extra_fields['auth_user']= [
  Field('latitude','float',default=0,label="Latitude of telescope",comment="South is negative, North is positive. For DMS, leave spaces in between to convert (try \"36 09 58\"). Leave this at zero if no telescope",requires=IS_FLOAT_IN_RANGE(-90, 90)),
  Field('longitude','float',default=0,label="Longitude of telescope",comment="West is negative, East is positive. For DMS, leave spaces in between to convert (try \"-86 47 02\"). Leave this at zero if no telescope",requires=IS_FLOAT_IN_RANGE(-180, 180)),
  Field('targetaltitude','float',default=20,label="Minimum altitude of target",comment="Minimum altitude that the telescope can observe in degrees relative to the horizon. Leave this at 20 if no telescope"),
  Field('sunaltitude','float',default=-5,label="Altitude of sun at dusk",comment="Altitude of sun at dusk and dawn in degrees, typically a negative number. Leave this at -5 if no telescope."),
  Field('timezone','string',default='(use local time zone)',requires=IS_IN_SET(["(use local time zone)","Etc/UTC","Africa/Abidjan","Africa/Algiers","Africa/Cairo","Africa/Casablanca","Africa/Johannesburg","Africa/Khartoum","Africa/Lagos","Africa/Maputo","Africa/Tripoli","Africa/Windhoek","America/Adak","America/Anchorage","America/Araguaina","America/Asuncion","America/Bahia","America/Campo_Grande","America/Cancun","America/Caracas","America/Chicago","America/Chihuahua","America/Denver","America/Fort_Nelson","America/Fortaleza","America/Godthab","America/Grand_Turk","America/Halifax","America/Havana","America/La_Paz","America/Lima","America/Los_Angeles","America/Managua","America/Metlakatla","America/Mexico_City","America/Miquelon","America/Montevideo","America/New_York","America/Noronha","America/Panama","America/Phoenix","America/Port-au-Prince","America/Rio_Branco","America/Santiago","America/Santo_Domingo","America/Sao_Paulo","America/St_Johns","Antarctica/Casey","Antarctica/Davis","Antarctica/Palmer","Antarctica/Troll","Asia/Amman","Asia/Baghdad","Asia/Baku","Asia/Bangkok","Asia/Barnaul","Asia/Beirut","Asia/Chita","Asia/Colombo","Asia/Damascus","Asia/Dhaka","Asia/Dili","Asia/Dubai","Asia/Famagusta","Asia/Gaza","Asia/Hong_Kong","Asia/Hovd","Asia/Irkutsk","Asia/Jakarta","Asia/Jayapura","Asia/Jerusalem","Asia/Kabul","Asia/Kamchatka","Asia/Karachi","Asia/Kathmandu","Asia/Kolkata","Asia/Krasnoyarsk","Asia/Magadan","Asia/Makassar","Asia/Manila","Asia/Novosibirsk","Asia/Omsk","Asia/Pyongyang","Asia/Rangoon","Asia/Sakhalin","Asia/Seoul","Asia/Shanghai","Asia/Srednekolymsk","Asia/Tashkent","Asia/Tehran","Asia/Tokyo","Asia/Tomsk","Asia/Ulaanbaatar","Asia/Vladivostok","Asia/Yakutsk","Asia/Yekaterinburg","Atlantic/Azores","Atlantic/Cape_Verde","Australia/Adelaide","Australia/Brisbane","Australia/Darwin","Australia/Eucla","Australia/Lord_Howe","Australia/Perth","Australia/Sydney","Etc/GMT+12","Etc/GMT+7","Etc/GMT-1","Etc/GMT-2","Etc/UCT","Europe/Astrakhan","Europe/Athens","Europe/Chisinau","Europe/Dublin","Europe/Istanbul","Europe/Kaliningrad","Europe/Lisbon","Europe/London","Europe/Moscow","Europe/Paris","Europe/Saratov","Europe/Simferopol","Europe/Volgograd","MET","Pacific/Apia","Pacific/Auckland","Pacific/Bougainville","Pacific/Chatham","Pacific/Easter","Pacific/Fakaofo","Pacific/Fiji","Pacific/Galapagos","Pacific/Gambier","Pacific/Guadalcanal","Pacific/Guam","Pacific/Honolulu","Pacific/Kiritimati","Pacific/Marquesas","Pacific/Niue","Pacific/Norfolk","Pacific/Pago_Pago","Pacific/Pitcairn","Pacific/Port_Moresby","Pacific/Tahiti","Pacific/Tongatapu"])),
  Field('format_12hr','boolean',default=False,label='Use 12 hour time (am/pm) instead of 24 hour time'),
  Field('api_key','string',readable=False,writable=False,default=(''.join(random.choice("0123456789abcdef") for x in range(32)))),
  Field('api_key_b64','string',readable=False,writable=False,default=""),
  Field('api_requests','integer',readable=False,writable=False,default=0),
  Field('api_last_request','datetime',readable=False,writable=False,default=request.now),]
service = Service()
plugins = PluginManager()

API_LIMIT = 1000
API_PERIOD = datetime.timedelta(hours=1)

# -------------------------------------------------------------------------
# create all tables needed by auth if not custom tables
# -------------------------------------------------------------------------
auth.define_tables(username=False, signature=False)
auth.settings.actions_disabled.append('reset_password')
auth.settings.actions_disabled.append('request_reset_password')


# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging'#if request.is_local else myconf.get('smtp.server')
mail.settings.sender = ''#myconf.get('smtp.sender')
mail.settings.login = ''#myconf.get('smtp.login')
mail.settings.tls = ''#myconf.get('smtp.tls') or False
mail.settings.ssl = ''#myconf.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)

mail.settings.server = settings.email_server
mail.settings.sender = settings.email_sender
mail.settings.login = settings.email_login


db.define_table('targets',
    Field('star_name', type='string',
          label=T('Star Name'),required=True),
    Field('zra', type='double',
          label=T('Ra'), writable=False, readable=False),
    Field('zdec', type='double',
          label=T('Dec'), writable=False, readable=False),
    Field('var_type', type='string',
          label=T('Var Type'), writable=False, readable=False),
    Field('min_mag', type='float',
          label=T('Minimum Magnitude'), writable=False, readable=False),
    Field('min_mag_band', type='string',
          label=T('Minimum Magnitude Band'), writable=False, readable=False),
    Field('max_mag', type='float',
          label=T('Maximum Magnitude'), writable=False, readable=False),
    Field('max_mag_band', type='string',
          label=T('Maximum Magnitude Band'), writable=False, readable=False),
    Field('period', type='double',
          label=T('Period'), writable=False, readable=False),
    Field('obs_cadence', type='double',
          label=T('Observing Cadence (d)'),comment="If omitted, this will be 10% of the period."),
    Field('obs_mode', type='string',
          label=T('Observing Mode'),requires=IS_IN_SET(('Photometry', 'Visual', 'Spectroscopy', 'All')),default='All',readable=False,writable=False),
    Field('obs_section', type='list:string', comment="Hold the Ctrl key and click to select multiple types. On a Mac, hold the Command key and click.",
          label=T('Observing Section'),requires=IS_IN_SET(('Alerts / Campaigns', 'Cataclysmic Variables', 'Eclipsing Variables', 'Short Period Pulsators', 'Long Period Variables', 'Young Stellar Objects', 'High Energy Targets', 'Exoplanets', "Miscellaneous", 'None'),multiple=True),default='None',required=True),
    Field('zfilter', type='string',
          label=T('Filter/Mode'),requires=IS_IN_SET(('B', 'V', 'R', 'I', 'U', 'J', 'H', 'Vis', 'Visual', 'Spectroscopy', 'All')),default='All'),
    Field('other_info', type='text',
          label=T('Other Info')),
    Field('last_data_point', type='datetime',
          label=T('Last Data Point'), writable=False, readable=False),
    Field('vsx_url', type='string',
          label=T('VSX URL'), writable=False, readable=False),
    Field('priority', type='boolean',
          label=T('High Priority')),
    Field('constellation', type='string',
          label=T('Constellation'), writable=False, readable=False),
    #Field('observable', type='boolean',
    #      label=T('Currently Observable'), writable=False, readable=False),
    auth.signature,
    format='%(star_name)s',
    migrate=settings.migrate)

db.define_table('targets_archive',db.targets,Field('current_record','reference targets',readable=False,writable=False))

db.define_table('feedback',
    Field("name","string",required=True),
    Field("email","string",required=True),
    Field("zmessage","text",label="Message"),
    Field('posted','datetime',default=request.now,writable=False,readable=False),migrate=settings.migrate)
    
db.define_table('announcement',
    Field("zmessage","string",required=True),
    auth.signature,
    migrate=settings.migrate)