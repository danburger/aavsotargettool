response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
response.logo = A("AAVSO Target Tool",
                  _class="navbar-brand", _href="/aavso", _style="font-size:30px;text-shadow: 0px 0px 10px black, 0px 0px 20px darkblue;",
                  _id="aavso-logo")


if auth.has_membership("admin") and auth.has_membership("owner"):
    response.menu = [
    (I(_class="fa fa-edit")+T(' Manage Targets'),URL('default','tool')==URL(),URL('default','tool'),[
        (T('Add Target'),URL('default','tool')==URL(),URL('default','tool'),[]),
        (T('Delete Multiple Targets'),URL('default','delete_multiple')==URL(),URL('default','delete_multiple'),[]),    
    ]),
    (I(_class="fa fa-users")+T(' User List'),URL('default','userlist')==URL(),URL('default','userlist'),[]),
    ]
elif auth.has_membership("admin"):
    response.menu = [
    (I(_class="fa fa-edit")+T(' Manage Targets'),URL('default','tool')==URL(),URL('default','tool'),[
        (T('Add Target'),URL('default','tool')==URL(),URL('default','tool'),[]),
        (T('Delete Multiple Targets'),URL('default','delete_multiple')==URL(),URL('default','delete_multiple'),[]),    
    ]),
    ]
elif auth.has_membership("owner"):
    response.menu = [
    (I(_class="fa fa-users")+T(' User List'),URL('default','userlist')==URL(),URL('default','userlist'),[]),
    ]
else:
    response.menu = []