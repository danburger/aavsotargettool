#!/usr/bin/python
import datetime

def get_targets(userid=auth.user_id,user=auth.user,vars=request.vars,usetime=datetime.datetime.utcnow()):
    if vars.select:
        session.select = vars.select
    if vars.orderby:
        session.orderby = vars.orderby
        session.reverse = vars.reverse
    if vars.settype:
        session.ac = vars.ac
        session.exo = vars.exo
        session.cv = vars.cv
        session.ev = vars.ev
        session.spv = vars.spv
        session.lpv = vars.lpv
        session.yso = vars.yso
        session.het = vars.het
        session.misc = vars.misc
    
    target_set = db.targets.id == 0 # dummy request
    if session.ac:
        target_set = target_set | db.targets.obs_section.contains("Alerts / Campaigns")
    if session.exo:
        target_set = target_set | db.targets.obs_section.contains("Exoplanets")
    if session.cv:
        target_set = target_set | db.targets.obs_section.contains("Cataclysmic Variables")
    if session.ev:
        target_set = target_set | db.targets.obs_section.contains("Eclipsing Variables")
    if session.spv:
        target_set = target_set | db.targets.obs_section.contains("Short Period Pulsators")
    if session.lpv:
        target_set = target_set | db.targets.obs_section.contains("Long Period Variables")
    if session.yso:
        target_set = target_set | db.targets.obs_section.contains("Young Stellar Objects")
    if session.het:
        target_set = target_set | db.targets.obs_section.contains("High Energy Targets")
    if session.misc:
        target_set = target_set | db.targets.obs_section.contains("Miscellaneous")
    if not (session.ac or session.exo or session.cv or session.ev or session.spv or session.lpv or session.yso or session.het or session.misc):
        target_set = target_set | db.targets.obs_section.contains("Alerts / Campaigns")
        default_types = True
    else:
        default_types = False
    if session.ac and session.exo and session.cv and session.ev and session.spv and session.lpv and session.yso and session.het and session.misc:
        everything = True
        target_set = db.targets.id > 0
    else:
        everything = False
        
    if session.orderby == "flag_color" and session.reverse:
        targets = db(target_set).select()
        targets = targets.sort(lambda row: get_flag_color(row.zra,row.zdec,row.last_data_point,row.obs_cadence),reverse=True)
        caretdown = None
    elif session.orderby == "flag_color":
        targets = db(target_set).select()
        targets = targets.sort(lambda row: get_flag_color(row.zra,row.zdec,row.last_data_point,row.obs_cadence))
        caretdown = session.orderby
    elif session.orderby == "priority" and session.reverse:
        targets = db(target_set).select()
        targets = targets.sort(lambda row: row[session.orderby])
        caretdown = None
    elif session.orderby == "priority":
        targets = db(target_set).select()
        targets = targets.sort(lambda row: row[session.orderby],reverse=True)
        caretdown = session.orderby
    elif session.orderby and session.reverse:
        targets = db(target_set).select()
        if session.orderby == "zfilter":
            targets = targets.sort(lambda row: row['zfilter']+"--"+row["star_name"],reverse=True)
        elif session.orderby == "star_name":
            targets = targets.sort(lambda row: row['star_name']+"--"+row["zfilter"],reverse=True)
        else:
            targets = targets.sort(lambda row: row[session.orderby],reverse=True)
        caretdown = None
    elif session.orderby:
        targets = db(target_set).select()
        if session.orderby == "zfilter":
            targets = targets.sort(lambda row: row['zfilter']+"--"+row["star_name"])
        elif session.orderby == "star_name":
            targets = targets.sort(lambda row: row['star_name']+"--"+row["zfilter"])
        else:
            targets = targets.sort(lambda row: row[session.orderby])
        caretdown = session.orderby
    else:
        targets = db(target_set).select()
        caretdown = None
        
    observabilityTimes = {}
    sundata = None
    if userid:
        sundata = getSunriseSunsetTimes((user.latitude,user.longitude),sunHorizon=user.sunaltitude,usetime=usetime)
        for row in targets:
            observabilityTimes[row.star_name] = getObservabilityTimes(
                (user.latitude,user.longitude),
                (row.zra,row.zdec),
                sundata,
                (user.get("targetaltitude") or 0),
                usetime)
        
    if session.select == "observable":
        if not userid or (user.latitude == 0 and user.longitude == 0):
            session.select = None
            redirect(URL('index'))
        targets.exclude(lambda row: len(observabilityTimes[row.star_name]) == 0)
            #get_altitude(row.zra,row.zdec,user.latitude, user.longitude) < (user.get('targetaltitude') or 0))
        
    refresh_task = db(db.scheduler_task.task_name=="refresh_targets").select().first()
        
    return dict(targets = targets, 
                caretdown = caretdown, 
                default_types=default_types, 
                everything=everything, 
                refresh_task=refresh_task,
                sundata=sundata,
                observabilityTimes=observabilityTimes)