import math
import ephem, datetime, time
from jdutil import datetime_to_jd
from gluon.contrib.markmin import markmin2html
try:
    from mailjet_rest import Client
    FG_HAS_MAILJET = True
except:
    FG_HAS_MAILJET = False

PI = 3.14159265358979323846264338327950288

#http://stackoverflow.com/questions/6870743/calculation-star-position-in-the-sky-pyephem
def get_altitude(ra, dec, lat, lon):
    observer = ephem.Observer()
    observer.lat = lat*(PI/180)
    observer.lon = lon*(PI/180)
    star = ephem.FixedBody()
    star._ra = ra*(PI/180)
    star._dec = dec*(PI/180)
    star.compute(observer)
    return float(star.alt*(180/PI))
    
SUN_IS_UP = 1
SUN_IS_DOWN = 2
SUN_IS_ALWAYS_UP = 3
SUN_IS_ALWAYS_DOWN = 4
SUN_ENTERS_ALWAYS_DOWN = 5
SUN_EXITS_ALWAYS_DOWN = 6
def getSunriseSunsetTimes(observerCoordinates,sunHorizon=0,usetime=datetime.datetime.utcnow()):
    observer = ephem.Observer()
    latitude = observerCoordinates[0]
    longitude = observerCoordinates[1]
    observer.lat = latitude * (PI/180)
    observer.lon = longitude * (PI/180)
    observer.horizon = (sunHorizon or 0) * (PI/180)
    observer.date = usetime
    sun = ephem.Sun()
    sun.compute(observer)
    try:
        if sun.alt > ((sunHorizon or 0) * PI/180):
            sunstate = SUN_IS_UP
            sunset = observer.next_setting(sun)
        else:
            sunstate = SUN_IS_DOWN
            sunset = observer.previous_setting(sun)
    except ephem.NeverUpError:
        try:
            sunrise = observer.next_rising(sun)
            return (SUN_EXITS_ALWAYS_DOWN,None,sunrise)
        except ephem.NeverUpError:
            return (SUN_IS_ALWAYS_DOWN,None,None)
    except ephem.AlwaysUpError:
        return (SUN_IS_ALWAYS_UP,None,None)
    try:
        sunrise = observer.next_rising(sun)
    except ephem.NeverUpError:
        return (SUN_ENTERS_ALWAYS_DOWN,sunset,None)
    return (sunstate, sunset, sunrise)
    
TARGET_RISES = 1
TARGET_SETS = 2
TARGET_IS_ALWAYS_UP = 3
TARGET_IS_ALWAYS_DOWN = 4
def getObservabilityTimes(observerCoordinates,targetCoordinates,sunriseSunsetTimes,minAltitude=0,usetime=datetime.datetime.utcnow()):
    latitude = observerCoordinates[0]
    longitude = observerCoordinates[1]
    ra = targetCoordinates[0]
    dec = targetCoordinates[1]
    if angsep(datetime_to_jd(datetime.datetime.utcnow()),ra,dec) < 30:
        #near solar conjunction
        return []
    sunstate = sunriseSunsetTimes[0]
    sunset = sunriseSunsetTimes[1]
    sunrise = sunriseSunsetTimes[2]
    observer = ephem.Observer()
    observer.lat = latitude * (PI/180)
    observer.lon = longitude * (PI/180)
    observer.horizon = (minAltitude or 0) * (PI/180)
    observer.date = usetime
    target = ephem.FixedBody()
    target._ra = ra*(PI/180)
    target._dec = dec*(PI/180)
    target.compute(observer)
    try:
        targetRise = observer.next_rising(target)
        targetRiseStatus = TARGET_RISES
    except ephem.AlwaysUpError:
        targetRise = 0
        targetRiseStatus = TARGET_IS_ALWAYS_UP
    except ephem.NeverUpError:
        targetRise = 0
        targetRiseStatus = TARGET_IS_ALWAYS_DOWN
    try:
        targetSet = observer.next_setting(target)
        targetSetStatus = TARGET_SETS
    except ephem.AlwaysUpError:
        targetSet = 0
        targetSetStatus = TARGET_IS_ALWAYS_UP
    except ephem.NeverUpError:
        targetSet = 0
        targetSetStatus = TARGET_IS_ALWAYS_DOWN
    if sunstate == SUN_IS_UP or sunstate == SUN_IS_DOWN:
        if targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_UP:
            return [(TARGET_IS_ALWAYS_UP,None)]
        elif targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_DOWN:
            return []
        elif 0 < sunset < targetRise < targetSet < sunrise:
            return [(TARGET_RISES,targetRise),(TARGET_SETS,targetSet)]
        elif 0 < sunset < targetSet < targetRise < sunrise:
            return [(TARGET_SETS,targetSet),(TARGET_RISES,targetRise)]
        elif 0 < sunset < targetRise < sunrise:
            return [(TARGET_RISES,targetRise)]
        elif 0 < sunset < targetSet < sunrise:
            return [(TARGET_SETS,targetSet)]
        elif 0 < sunset < sunrise < targetRise < targetSet:
            return []
        elif 0 < sunset < sunrise < targetSet < targetRise:
            return [(TARGET_IS_ALWAYS_UP,None)]
        elif 0 < targetSet < sunset < sunrise < targetRise:
            return []
        elif 0 < targetRise < sunset < sunrise < targetSet:
            return [(TARGET_IS_ALWAYS_UP,None)]
        elif 0 < targetSet < targetRise < sunset < sunrise:
            return [(TARGET_IS_ALWAYS_UP,None)]
        elif 0 < targetRise < targetSet < sunset < sunrise:
            return []
        else:
            return []
    elif sunstate == SUN_IS_ALWAYS_UP:
        return []
    elif sunstate == SUN_IS_ALWAYS_DOWN:
        if targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_UP:
            return [(TARGET_IS_ALWAYS_UP,None)]
        elif targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_DOWN:
            return []
        elif 0 < targetRise < targetSet:
            return [(TARGET_RISES,targetRise),(TARGET_SETS,targetSet)]
        elif 0 < targetSet < targetRise:
            return [(TARGET_SETS,targetSet),(TARGET_RISES,targetRise)]
        elif 0 < targetRise:
            return [(TARGET_RISES,targetRise)]
        elif 0 < targetSet:
            return [(TARGET_SETS,targetSet)]
        else:
            return []
    elif sunstate == SUN_ENTERS_ALWAYS_DOWN:
        if targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_UP:
            return [(TARGET_IS_ALWAYS_UP,None)]
        elif targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_DOWN:
            return []
        elif 0 < sunset < targetRise < targetSet:
            return [(TARGET_RISES,targetRise),(TARGET_SETS,targetSet)]
        elif 0 < sunset < targetSet < targetRise:
            return [(TARGET_SETS,targetSet),(TARGET_RISES,targetRise)]
        elif 0 < sunset < targetRise:
            return [(TARGET_RISES,targetRise)]
        elif 0 < sunset < targetSet:
            return [(TARGET_SETS,targetSet)]
        else:
            return []
    elif sunstate == SUN_EXITS_ALWAYS_DOWN:
        if targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_UP:
            return [(TARGET_IS_ALWAYS_UP,None)]
        elif targetRiseStatus == targetSetStatus == TARGET_IS_ALWAYS_DOWN:
            return []
        elif 0 < targetRise < targetSet < sunrise:
            return [(TARGET_RISES,targetRise),(TARGET_SETS,targetSet)]
        elif 0 < targetSet < targetRise < sunrise:
            return [(TARGET_SETS,targetSet),(TARGET_RISES,targetRise)]
        elif 0 < targetRise < sunrise:
            return [(TARGET_RISES,targetRise)]
        elif 0 < targetSet < sunrise:
            return [(TARGET_SETS,targetSet)]
        else:
            return []

# Converted from code by John G O'Neill 6/3/16
def angsep(jd, r2, d2):
    #Constants:
    limit = 30
    p2 = 6.283185307
    coseps = 0.91748
    sineps = 0.39778
    a = 0.993133
    b = 99.997361

    #Computations:
    t = (jd - 2451545.0)/36525
    frac, whole = math.modf(a + (b * t))
    m = p2 * frac
    dl = 6893.0 * math.sin(m) + 72.0 * math.sin(2 * m)
    frac2, whole2 = math.modf(0.7859453 + m/p2 + (6191.2 * t + dl)/1296E3)
    l = p2 * frac2
    sl = math.sin(l)
    x = math.cos(l)
    y = coseps * sl
    z = sineps * sl
    rho = math.sqrt(1.0 - z*z)
    d1 = (360.0/p2) * math.atan(z/rho)
    ra_hours = (48.0/p2) * math.atan(y/(x+rho)) 

    #Check to see if RA hours is negative:
    if (ra_hours < 0):
        ra_hours = ra_hours + 24.0

    #Convert RA to decimal:
    r1 = ra_hours * 15

    #Now the angular separation of any star from the Sun can be computed:

    #Variables:
    #r1 = ra of Sun in decimal degrees
    #d1 = dec of Sun in decimal degrees
    #r2 = ra of star in decimal degrees
    #d2 = dec of star in decimal degrees

    ang_sep_rad = math.acos(math.sin(math.radians(d1)) * math.sin(math.radians(d2)) + math.cos(math.radians(d1)) * math.cos(math.radians(d2)) * math.cos(math.radians(r1 - r2)))

    ang_sep = math.degrees(ang_sep_rad)
    
    return ang_sep
    
def get_flag_color(ra, dec, last_data, cadence):
    if angsep(datetime_to_jd(datetime.datetime.utcnow()), ra, dec) < 30:
        return '5-black'
    elif not last_data:
        return '1-rednodata'
    elif datetime.datetime.utcnow() > (last_data + datetime.timedelta(days=cadence)):
        return '2-red-' + datetime.datetime.isoformat(last_data)
    elif datetime.datetime.utcnow() > (last_data + datetime.timedelta(days=cadence*0.7)):
        return '3-orange-' + datetime.datetime.isoformat(last_data)
    else:
        return '4-green-' + datetime.datetime.isoformat(last_data)
        
def add_targets(targets,obs_section,note,priority,queued=False):
    if not queued:
        scheduler.queue_task(add_targets,immediate=True,timeout=3600,pvars=dict(targets=targets,obs_section=obs_section,note=note,priority=priority,queued=True),sync_output=2)
        return "Added to scheduler"
    data_in = []
    for line in targets.splitlines():
        data_in.append(vsxAPI((line,obs_section)))
    notes = ""
    for info in data_in:
        if type(info) == str:
            notes += info
            continue
        if info['period']:
            obs_cadence = info['period']*0.1
        else:
            obs_cadence = 3
        if db(db.targets.star_name==info['name'])(db.targets.obs_section==obs_section).count() > 0:
            notes += ("Target " + info['name'] + ' already in database.')
            continue # target already in database
        db.targets.insert(star_name=info['name'],
                          zra = info['ra'],
                          zdec = info['dec'],
                          max_mag = info['maxMag'],
                          max_mag_band = info['maxMagBand'],
                          min_mag = info['minMag'],
                          min_mag_band = info['minMagBand'],
                          period = info['period'],
                          var_type = info['varType'],
                          last_data_point = info['obsJDate'],
                          obs_cadence = obs_cadence,
                          obs_section = obs_section,
                          other_info = note,
                          priority = priority,
                          vsx_url = 'https://www.aavso.org/vsx/index.php?view=detail.top&oid=' + str(info['OID']))
    db.commit()
    return "Done. " + notes
    
def refresh_targets(queued=False):
    if not queued:
        scheduler.queue_task(refresh_targets,timeout=3600,repeats=0,period=3600,pvars=dict(queued=True),sync_output=2,retry_failed=-1)
        return "Done"
    start_time = time.time()
    targets = db(db.targets.id > 0).select()
    print "len(targets) = %s" % len(targets)
    vsxdata = vsxAPI([(target.star_name,target.zfilter.replace("Visual","Vis.")) for target in targets])
    print "len(vsxdata) = %s" % len(vsxdata)
    notes = ""
    for i in range(len(vsxdata)):
        if type(vsxdata[i]) == str:
            notes += vsxdata[i]
            continue
        print repr(vsxdata[i])
        aid_target = vsxdata[i][0]['obsJDate']
        aid_constellation = vsxdata[i][0]['constellation']
        if type(aid_target) != str:
            print "Changing target %s to %s" % (targets[i].id,aid_target)
            db(db.targets.id == targets[i].id).update(last_data_point=aid_target,constellation=aid_constellation)
        else:
            print "Changing target %s to none" % (targets[i].id)
            db(db.targets.id == targets[i].id).update(last_data_point=None,constellation=aid_constellation)
    db.commit()
    return "Done, took %s seconds. %s" % (time.time()-start_time, notes)
    
#http://www.bdnyc.org/2012/10/decimal-deg-to-hms/
def deg2HMS(ra='', dec='', round=True, xml=False, csv=False):
  RA, DEC, rs, ds = '', '', '', '+'
  if dec:
    if str(dec)[0] == '-':
      ds, dec = '-', abs(dec)
    deg = int(dec)
    decM = abs(int((dec-deg)*60))
    if round:
      decS = int((abs((dec-deg)*60)-decM)*60)
    else:
      decS = (abs((dec-deg)*60)-decM)*60
    if csv:
      DEC = '{0}{1}d {2:02g}m {3:02g}s'.format(ds, deg, decM, decS)  
    else:
      DEC = '{0}{1}° {2:02g}\' {3:02g}"'.format(ds, deg, decM, decS)
  
  if ra:
    if str(ra)[0] == '-':
      rs, ra = '-', abs(ra)
    raH = int(ra/15)
    raM = int(((ra/15)-raH)*60)
    if round:
      raS = int(((((ra/15)-raH)*60)-raM)*60)
    else:
      raS = ((((ra/15)-raH)*60)-raM)*60
    if xml:
      RA = '{0}{1:02g}<sup>h</sup> {2:02g}<sup>m</sup> {3:02g}<sup>s</sup>'.format(rs, raH, raM, raS)
      RA = XML(RA)
    else:
      RA = '{0}{1:02g}h {2:02g}m {3:02g}s'.format(rs, raH, raM, raS)
  
  if ra and dec:
    return (RA, DEC)
  else:
    return RA or DEC
    
def http_format(s):
    if not s:
        return ""
    return XML(
        markmin2html.markmin2html("\n\n".join(s.splitlines()))
            .replace("<a ","<a target='_blank' "),
        sanitize=True,
        permitted_tags=['a','p']
    )
    
def obs_section_cleanup(s):
    return str(s).replace('[','')\
                 .replace(']','')\
                 .replace("'","")\
                 .replace("Alerts / Campaigns","Alert/Campaign")\
                 .replace("Exoplanets","EP")\
                 .replace("Cataclysmic Variables","CV")\
                 .replace("Eclipsing Variables","EB")\
                 .replace("Short Period Pulsators","SPP")\
                 .replace("Long Period Variables","LPV")\
                 .replace("Young Stellar Objects","YSO")\
                 .replace("High Energy Targets","HET")\
                 .replace("Miscellaneous","Misc")
                 
def email_user(content,email,subject="A message from AAVSO Target Tool",link=None,button=None,safety=True):
    print email
    print subject
    print response.render("email.txt",dict(content=content,link=link,subject=subject,button=button))
    if safety:
        pass
    elif FG_HAS_MAILJET and FG_MAILJET_APIKEY:
        mailjet = Client(auth=(FG_MAILJET_APIKEY, FG_MAILJET_SECRETKEY))
        mj_email = {
            'FromName': 'AAVSO Target Tool',
            'FromEmail': FG_MAILJET_FROM,
            'Subject': subject,
            'Html-Part': response.render("email.html",dict(content=content,link=link,subject=subject,button=button)),
            'Text-Part': response.render("email.txt" ,dict(content=content,link=link,subject=subject,button=button)),
            'Recipients': [{'Email': email}]
        }
        mailjet.send.create(mj_email)
    else:
        htmlpart = response.render("email.html",dict(content=content,link=link,subject=subject,button=button)),
        textpart = response.render("email.txt" ,dict(content=content,link=link,subject=subject,button=button)),
        mail.send(email,subject,(textpart,htmlpart))
    return "Message sent."