from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'AAVSO Target Tool'
settings.subtitle = ''
settings.author = 'Dan Burger, Chandler Barnes and Kenneth Li'
settings.author_email = 'aavso@aavso.org'
settings.keywords = 'variable star observations'
settings.description = 'A web application for bringing stars in need of observation to the attention of AAVSO’s network of amateur and professional astronomers.'
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'e2ee63f0-6d6a-43d6-be88-06d6c9bc92f1'
settings.email_server = 'logging'
settings.email_sender = ''
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
